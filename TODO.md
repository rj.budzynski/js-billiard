* clean up code, esp. re: `Billiard.options` - Ok
* split out stuff from `main.js` - Ok
* abstract out the container shape
* split the dynamics out from the UI ?
* allow for different velocities - Ok
* include background force
* try threading (web workers)
* include two-body forces

## NOTES

* in the presence of a potential the bouncing violates energy conservation;
this can be mitigated by correcting the kinetic energy after each bounce.