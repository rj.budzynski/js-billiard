// definition of the Billiard class
// here it interacts directly only with its DOM container
// Billiard creates its own Canvas in an existing container

const { PI, sin, cos } = Math;

class Billiard {

    constructor(container, options = {}) {
        this.defaults = {
            bgcolor: 'black',
            alpha: .09,
            facecolor: 'magenta',
            edgecolor: 'red',
            edgewidth: 1,
            ballradius: 3,
            speed: 100, // pixels per second
            trails: true,
            g: 10,
        };
        options = { ...this.defaults, ...options }; // yes this return a new object that merges second into first
        Object.assign(this, options);
        // `container` should be a html element
        this.container = container;
        this.canvas = document.createElement('canvas');
        this.container.appendChild(this.canvas);
        let width = this.canvas.clientWidth, height = this.canvas.clientHeight;
        this.canvas.setAttribute('width', parseInt(width));
        this.canvas.setAttribute('height', parseInt(height));
        this.canvas.style.backgroundColor = this.bgcolor;
        this.width = this.canvas.width;
        this.height = this.canvas.height;
        this.gc = this.canvas.getContext('2d');
        this.gc.strokeStyle = this.edgecolor;
        this.gc.fillStyle = this.facecolor;
        this.gc.lineWidth = this.edgewidth;
    }

    clear(bgcolor) {
        this.gc.fillStyle = bgcolor || this.bgcolor;
        this.gc.fillRect(0, 0, this.width, this.height);
        this.gc.fillStyle = this.facecolor;
    }

    init(X, Y, V, Phi) {
        let rgb = getComputedStyle(this.canvas).backgroundColor;
        this.bgtranslucent = rgb.replace(/\)$/, `,${this.alpha})`);
        this.X = Float64Array.from(X);
        this.Y = Float64Array.from(Y);
        this.V = Float64Array.from(V);
        this.Phi = Float64Array.from(Phi);
        this.clear(this.bgcolor);
        this.draw();
        this.newFrame = this.newFrame.bind(this);
    }

    get nBalls() {
        return this.X && this.X.length;
    }

    draw() {
        this.gc.beginPath();
        for (let i = this.nBalls; i--;) {
            this.makeBall(this.X[i], this.Y[i]);
            if (i) {
                this.gc.moveTo(this.X[i - 1], this.Y[i - 1]);
            }
        }
        this.gc.fill();
    }

    makeBall(x, y) {
        this.gc.moveTo(x + this.ballradius, y);
        this.gc.arc(x, y, this.ballradius, 0, 2 * PI);
        this.gc.closePath();
    }

    isOutside(x, y) {
        return x < 0 || x > this.width || y < 0 || y > this.height;
    }

    step(dt) {
        let w = this.width,
            h = this.height;
        for (let i = this.nBalls; i--;) {
            let x = this.X[i],
                y = this.Y[i],
                v = this.V[i],
                phi = this.Phi[i];
            let vx = v * cos(phi),
                vy = v * sin(phi);
            let xnext = x + vx * dt,
                ynext = y + vy * dt;

            while (this.isOutside(xnext, ynext)) {
                switch (true) {
                    case xnext <= 0:
                        phi = PI - phi;
                        xnext *= -1;
                        break
                    case xnext >= w:
                        phi = PI - phi;
                        xnext = 2 * w - xnext;
                        break;
                    case ynext <= 0:
                        phi = 2 * PI - phi;
                        ynext *= -1;
                        break;
                    case ynext >= h:
                        phi = 2 * PI - phi;
                        ynext = 2 * h - ynext;
                        break;
                }
            }
            this.X[i] = xnext;
            this.Y[i] = ynext;
            this.Phi[i] = phi;
        }
    }

    newFrame(hrtime) {
        if (this.lastUpdate) {
            let dt = (hrtime - this.lastUpdate) / 1e3;
            this.step(dt);
            this.trails ? this.clear(this.bgtranslucent) : this.clear(this.bgcolor);
            this.draw();
        }
        if (this.running) {
            this.lastUpdate = hrtime;
            ++this._frames;
            window.requestAnimationFrame(this.newFrame);
        } else {
            this.lastUpdate = null;
        }
    }

    toggleAnimation() {
        if (!this.running) {
            this.running = true;
            this._fps = this._frames = 0;
            this.fpsTimer = setInterval((() => {
                this._fps = this._frames;
                this._frames = 0;
            }).bind(this), 1000);
            this.newFrame();
        } else {
            this.running = false;
            clearInterval(this.fpsTimer);
        }
    }

    get fps() {
        if (!this.running) return 0;
        return this._fps;
    }

}

export { Billiard };