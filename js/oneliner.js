window.addEventListener('load', () => {

    const urlBase = "https://raw.githubusercontent.com/rjbudzynski/oneliners/master/data/";
    const textbox = document.querySelector('#oneliner');

    function getResponseBody(response) {
        if (!response.ok) {
            throw new Error(`${response.url}: ${response.status} ${response.statusText}`);
        }
        return response.text()
    }

    async function getRandomOneliner() {
        const len = await fetch(urlBase + "len").then(getResponseBody).then(parseInt);
        const index = Math.floor(len * Math.random());
        const text = await fetch(urlBase + index).then(getResponseBody);
        textbox.innerText = text;
    }

    setInterval(getRandomOneliner, 120e3);
});