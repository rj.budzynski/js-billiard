// cached extra result
var spareValue = NaN

/**
 * random number generator for the unit random variable
 * http://en.wikipedia.org/wiki/Normal_distribution#Generating_values_from_normal_distribution
 * @returns {Number} random number
 */

// © Hugo Villeneuve; https://www.npmjs.com/package/random-z
// modified by rjb

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

const { random, log, sqrt } = Math;

/*module.exports = */
function randomZ() {
	var u, v, s

	if (!isNaN(spareValue)) {
		s = spareValue
		spareValue = NaN
		return s
	}

	do {
		u = random() * 2 - 1
		v = random() * 2 - 1
		s = u * u + v * v
	} while (s === 0 || s >= 1)

	s = sqrt(-2 * log(s) / s)
	spareValue = u * s

	return v * s
}

export { randomZ };