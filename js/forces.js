import { Billiard } from './billiard.js';
const { sqrt, sin, cos, PI, atan2, hypot } = Math;

// billiard with forces

class HeavyGas extends Billiard {

    step(dt) {
        let w = this.width,
            h = this.height;
        let X = this.X, Y = this.Y, V = this.V, Phi = this.Phi;
        for (let i = this.nBalls; i--;) {
            let e;  // for energy correction at bounce
            let vx = V[i] * cos(Phi[i]),
                vy = V[i] * sin(Phi[i]);
            X[i] += vx * dt;
            Y[i] += vy * dt + this.g * dt * dt * .5;
            vy += this.g * dt;
            Phi[i] = atan2(vy, vx);
            V[i] = hypot(vx, vy);
            // deal with a possible bounce
            let bounced = false;
            while (this.isOutside(X[i], Y[i])) {
                switch (true) {
                    case X[i] <= 0:
                        Phi[i] = PI - Phi[i];
                        X[i] *= -1;
                        break
                    case X[i] >= w:
                        Phi[i] = PI - Phi[i];
                        X[i] = 2 * w - X[i];
                        break;
                    case Y[i] <= 0:
                        e = HeavyGas.singleEnergy(X[i], Y[i], V[i], this.g);
                        Phi[i] = 2 * PI - Phi[i];
                        Y[i] *= -1;
                        bounced = true;
                        break;
                    case Y[i] >= h:
                        e = HeavyGas.singleEnergy(X[i], Y[i], V[i], this.g);
                        Phi[i] = 2 * PI - Phi[i];
                        Y[i] = 2 * h - Y[i];
                        bounced = true;
                        break;
                }
            }
            if (bounced) {
                let de = HeavyGas.singleEnergy(X[i], Y[i], V[i], this.g) - e;
                V[i] = sqrt(V[i] * V[i] - 2 * de);   // correct for energy violation
            }
        }
    }

    get energy() {
        let e = 0;
        for (let i = this.nBalls; i--;) {
            e += .5 * this.V[i] ** 2 - this.Y[i] * this.g;
        }
        return e + this.nBalls * this.height * this.g;
    }

    static singleEnergy(x, y, v, g) {
        return .5 * v * v - y * g;
    }

}

export { HeavyGas };