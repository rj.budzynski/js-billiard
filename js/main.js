// import { Billiard } from './billiard.js';
import { HeavyGas } from './forces.js';
import { $ } from './functions.js';
import { randomZ } from './random-z.js';
import './events.js';
import './oneliner.js';

export { billiard };  // needs to be globally available

const { sin, cos, atan2, hypot, random, PI } = Math;

// ACTION!

let opts = {};
let queryString = decodeURI(document.location.search);
if (queryString) {
    let params = queryString.substring(1).split(/[;&]/);
    params.forEach((p) => {
        let [name, val, ..._] = p.split('=');
        opts[name] = val;
    });
}

let nPoints = 255,
    n = parseInt(opts.n),
    options = {};
if (n)
    if (n > 1 && n < $('input[type=number]').max) nPoints = n;
    else alert('invalid value for nPoints in query string!');
delete opts.n;
let trails = (parseInt(opts.t) == 1);
if (trails)
    options.trails = true;
delete opts.t;
['ballradius', 'edgewidth', 'speed'].forEach((key) => {
    if (key in opts)
        opts[key] = opts[key] | 0;
});
if ('alpha' in opts)
    opts.alpha = parseFloat(opts.alpha)
Object.assign(options, opts);


const billiard = new HeavyGas($('#canvas-container0'), options);


let w = billiard.canvas.width,
    h = billiard.canvas.height,
    r = billiard.ballradius;

// Gaussian distributed velocities:

const randV = () => {
    let vx = billiard.speed * .5 * randomZ(),
        vy = billiard.speed * .5 * randomZ();
    return hypot(vx, vy);
};


let X = Array.from({ length: nPoints }, () => r + (w - 2 * r) * random()),
    Y = Array.from({ length: nPoints }, () => r + (h - 2 * r) * random()),
    V = Array.from({ length: nPoints }, randV),
    Phi = Array.from({ length: nPoints }, () => 2 * PI * random());

billiard.init(X, Y, V, Phi);

$('#nballs').innerText = billiard.nBalls;
setInterval(() => {
    $('#fps').innerText = billiard.fps;
    // $('#energy').innerText = billiard.energy |0;
}, 1700);
