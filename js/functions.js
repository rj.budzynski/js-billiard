// misc functions that do not depend on the specific DOM layout,
// only on the Billiard and its properties

// for non-module running:
// const { sqrt, log, sin, cos, atan2, hypot, random, PI } = Math;
// comment out if module

const $ = document.querySelector.bind(document);

const bigBang = (billiard) => {
    collapse(billiard);
    billiard.gc.font = 'bold 128px "Courier New", Courier, monospace';
    billiard.gc.textBaseline = 'middle';

    for (let i = 0; i < 5; ++i) {
        setTimeout(() => {
            billiard.clear();
            if (i == 4) {
                billiard.gc.fillStyle = billiard.facecolor;
                billiard.draw();
                billiard.toggleAnimation();
                return;
            }
            billiard.gc.fillStyle = 'orange';
            billiard.gc.fillText(`${3 - i}`, .45 * billiard.width, .5 * billiard.height);
        }, (i + 4) * 1000);
    }
}


const warp = (billiard, X, Y, Phi) => {
    let nFrames = 120;
    const deltaX = billiard.X.map((x, i) => (X[i] - x) / nFrames);
    const deltaY = billiard.Y.map((y, i) => (Y[i] - y) / nFrames);
    Phi.forEach((p, i) => {
        billiard.Phi[i] = p;
    });
    const frame = () => {
        // console.log(nFrames);
        billiard.clear('rgb(0,0,0,.13');
        deltaX.forEach((dx, i) => { billiard.X[i] += dx; });
        deltaY.forEach((dy, i) => { billiard.Y[i] += dy; });
        billiard.draw();
        if (--nFrames) window.requestAnimationFrame(frame);
    };
    window.requestAnimationFrame(frame);
}

const collapse = (billiard) => {
    let nFrames = 120;
    const x0 = billiard.width / 2;
    const y0 = billiard.height / 2;
    const frame = () => {
        billiard.clear(billiard.bgtranslucent);
        for (let i = billiard.nBalls; i--;) {
            billiard.X[i] = billiard.X[i] * .95 + x0 * .05;
            billiard.Y[i] = billiard.Y[i] * .95 + y0 * .05;
        }
        billiard.draw();
        if (--nFrames) window.requestAnimationFrame(frame);
    };
    window.requestAnimationFrame(frame);
};

export { $, bigBang, collapse };