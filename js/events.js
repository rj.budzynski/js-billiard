// event handlers made for a specific GUI layout
// they will need a billiard:Billiard instance when activated

import { $, bigBang } from './functions.js';
import { billiard } from './main.js';

const SPEEDFACTOR = Math.sqrt(2);

$('#howmany #ok').addEventListener('click', () => {
    // let url = document.location.pathname;
    let inp = $('#howmany input');
    let n;
    if (inp.reportValidity()) {
        n = inp.value;
    } else {
        alert('Invalid value!');
        this.style.display = 'none';
        return;
    }
    let queryString = `?n=${n}`;
    if (billiard.trails) queryString += ';t=1';
    for (let key of ['bgcolor', 'facecolor', 'ballradius', 'speed', 'alpha']) {
        if (key in billiard) {
            if (billiard[key] !== billiard.defaults[key]) {
                queryString += encodeURI(`;${key}=${billiard[key]}`);
            }
        }
    }
    document.location.search = queryString;
});

$('#howmany #cancel').addEventListener('click', () => {
    $('#howmany').style.display = 'none';
});

document.addEventListener('keydown', (ev) => {
    // console.log(ev.key);
    if (ev.key == 'Escape') {
        $('#howmany').style.display = 'none';
    }
});

window.addEventListener('keypress', (ev) => {
    // console.log(ev.key);
    switch (ev.key) {
        case ' ':
            billiard.toggleAnimation();
            $('#status').innerText = billiard.running ? 'running' : 'stopped';
            break;
        case '+':
            billiard.speed *= SPEEDFACTOR;
            if (billiard.V) {
                for (let i = billiard.nBalls; i--;) {
                    billiard.V[i] *= SPEEDFACTOR;
                }
            }
            break;
        case '-':
            billiard.speed /= SPEEDFACTOR;
            if (billiard.V) {
                for (let i = billiard.nBalls; i--;) {
                    billiard.V[i] /= SPEEDFACTOR;
                }
            }
            break;
        case 't':
            billiard.trails = !billiard.trails;
            break;
        case 'r':
            $('#howmany input[type=number]').value = '' + billiard.nBalls;
            console.log(billiard.nBalls);
            $('#howmany').style.display = 'block';
            break;
        case 'b':
            if (billiard.running) {
                billiard.toggleAnimation();
                $('#status').innerText = 'stopped';
            }
            bigBang(billiard);
            $('#status').innerText = 'running';
            break;
    }
});

{  // avoid littering global namespace
    const activator = $('#menuactivator');
    const descDiv = $('#description');
    descDiv.miniStyle = {
        width: '0px',
        height: '0px',
        color: 'rgba(0,0,0,0)',
        borderColor: 'rgba(0,0,0,0)',
    };
    descDiv.originalStyle = {};
    let oStyle = getComputedStyle(descDiv);
    for (let key in descDiv.miniStyle) {
        descDiv.originalStyle[key] = oStyle[key];
    }
    for (let key in descDiv.originalStyle) {
        descDiv.style[key] = descDiv.originalStyle[key];
    }
    descDiv.mini = false;
    activator.addEventListener('click', () => {
        if (descDiv.mini) {
            descDiv.mini = false;
            for (let key in descDiv.originalStyle) {
                descDiv.style[key] = descDiv.originalStyle[key];
            }

        } else {
            for (let key in descDiv.miniStyle) {
                descDiv.style[key] = descDiv.miniStyle[key];
            }
            descDiv.mini = true;
        }
    });
}